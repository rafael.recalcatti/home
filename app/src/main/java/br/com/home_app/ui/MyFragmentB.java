package br.com.home_app.ui;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import br.com.home_app.MainActivity;
import br.com.home_app.R;
import br.com.home_app.dto.HomeControlDto;
import br.com.home_app.utils.LoadHomeControl;
import br.com.home_app.utils.SaveHomeControl;


public class MyFragmentB extends Fragment {


    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private View root;
    private String actualFragment = null;
    private HomeControlDto homeControlDto = null;
    private Integer statusGarageDoor = null;
    private Boolean flag = true;

    public MyFragmentB() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_my_b, container, false);
        executor = ContextCompat.getMainExecutor(getActivity());

        try {
            homeControlDto = new LoadHomeControl(null).execute().get(10, TimeUnit.SECONDS);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        final ToggleButton toggleButtonPortaoGaragem = root.findViewById(R.id.toggle_button_garage);
        toggleButtonPortaoGaragem.setChecked(homeControlDto.getGarageDoorStatus().equals(0));

        biometricPrompt = new BiometricPrompt(this,
                executor, new BiometricPrompt.AuthenticationCallback() {

            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getActivity(),
                        "Erro na autenticação: " + errString, Toast.LENGTH_SHORT)
                        .show();
                //toggleButtonPortaoGaragem.setChecked(false);

            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                flag = true;
                Toast.makeText(getActivity(),
                        "Autenticação bem sucedida!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getActivity(), "Falha na autenticação!",
                        Toast.LENGTH_SHORT)
                        .show();
                //flag = true;
                //toggleButtonPortaoGaragem.setChecked(false);
            }

        });

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Biometria requerida")
                .setSubtitle("Para liberar o acesso")
                .setNegativeButtonText("Cancelar")
                .build();

        toggleButtonPortaoGaragem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                //if (isChecked) {
                //biometricPrompt.authenticate(promptInfo);
                //}

                if (flag) {
                    if (Objects.nonNull(homeControlDto)) {
                        if (toggleButtonPortaoGaragem.isChecked()) {
                            homeControlDto.setGarageDoorStatus(1);
                        } else {
                            homeControlDto.setGarageDoorStatus(0);
                        }
                    }

                    try {
                        if (Objects.nonNull(homeControlDto))
                            if (statusGarageDoor != homeControlDto.getGarageDoorStatus())
                                homeControlDto.setTokenGarageDoor(UUID.randomUUID().toString());
                        new SaveHomeControl(getActivity()).execute(homeControlDto).get(10, TimeUnit.SECONDS);

                    } catch (ExecutionException | InterruptedException | TimeoutException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        try {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    actualFragment = MainActivity.getActualFragment();
                    if (actualFragment.equals("FRAGMENT_B")) {
                        MyFragmentB fragmentB = new MyFragmentB();
                        managerFragment(fragmentB, "FRAGMENT_B");
                    }
                }
            }, 10000);
        } catch (Exception e) {

        }
        return root;
    }

    private void managerFragment(Fragment fragment, String tag) {
        try {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.flContainerForFragment, fragment, tag);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } catch (Exception e) {

        }
    }

}
