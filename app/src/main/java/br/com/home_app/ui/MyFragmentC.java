package br.com.home_app.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import br.com.home_app.MainActivity;
import br.com.home_app.R;
import br.com.home_app.dto.HomeControlDto;
import br.com.home_app.utils.LoadHomeControl;


public class MyFragmentC extends Fragment {

    private String actualFragment = null;

    public MyFragmentC() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_my_c, container, false);

        HomeControlDto homeControlDto = null;
        try {
            homeControlDto = new LoadHomeControl(null).execute().get(10, TimeUnit.SECONDS);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        final TextView textViewClimate1 = root.findViewById(R.id.textViewSensor1);
        final TextView textViewTemperature1 = root.findViewById(R.id.textViewTemperatureSensor1);
        final TextView textViewHumitidy1 = root.findViewById(R.id.textViewHumiditySensor1);

        textViewClimate1.setText("Temperatura: " + homeControlDto.getClimates().get(0).getDescription());
        textViewTemperature1.setText("Temperatura: " + homeControlDto.getClimates().get(0).getTemperature() + "°");
        textViewHumitidy1.setText("Temperatura: " + homeControlDto.getClimates().get(0).getHumidity() + "%");


        try {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    actualFragment = MainActivity.getActualFragment();
                    if (actualFragment.equals("FRAGMENT_C")) {
                        MyFragmentC fragmentC = new MyFragmentC();
                        managerFragment(fragmentC, "FRAGMENT_C");
                    }
                }
            }, 10000);
        } catch (Exception e) {

        }
        return root;
    }

    private void managerFragment(Fragment fragment, String tag) {
        try {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.flContainerForFragment, fragment, tag);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } catch (Exception e) {

        }
    }
}
