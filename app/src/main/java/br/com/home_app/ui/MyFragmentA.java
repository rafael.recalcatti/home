package br.com.home_app.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import br.com.home_app.MainActivity;
import br.com.home_app.R;
import br.com.home_app.dto.HomeControlDto;
import br.com.home_app.utils.LoadHomeControl;
import br.com.home_app.utils.SaveHomeControl;

public class MyFragmentA extends Fragment {

    private List<Integer> status = null;
    private HomeControlDto homeControlDto = null;
    private String actualFragment = null;

    public MyFragmentA() {

    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_my_a, container, false);


        final Switch aSwitchQuarto = root.findViewById(R.id.switch_light_quarto);

        final TextView textViewUsgaeTimeLight1 = root.findViewById(R.id.textViewTimeUsageLight1);

        final Switch aSwitchSala = root.findViewById(R.id.switch_light_sala);

        final TextView textViewUsgaeTimeLight2 = root.findViewById(R.id.textViewTimeUsageLight2);

        if (actualFragment == null || actualFragment.equals("FRAGMENT_A")) {
            try {
                status = new ArrayList<>();
                homeControlDto = new LoadHomeControl(null).execute().get(10, TimeUnit.SECONDS);
            } catch (ExecutionException | InterruptedException | TimeoutException e) {
                e.printStackTrace();
            }
        }

        if (Objects.nonNull(homeControlDto))
            homeControlDto.getLights().stream().forEach(homeControlDto ->
                    status.add(homeControlDto.getStatus())
            );

        aSwitchSala.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Objects.nonNull(homeControlDto))
                    if (isChecked) {
                        homeControlDto.getLights().get(0).setStatus(1);
                    } else {
                        homeControlDto.getLights().get(0).setStatus(0);
                    }

                try {
                    if (Objects.nonNull(homeControlDto))
                        if (status.get(0) != homeControlDto.getLights().get(0).getStatus())
                            new SaveHomeControl(getActivity()).execute(homeControlDto).get(10, TimeUnit.SECONDS);

                } catch (ExecutionException | InterruptedException | TimeoutException e) {
                    e.printStackTrace();
                }
            }
        });

        aSwitchQuarto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Objects.nonNull(homeControlDto)) {

                    if (isChecked) {
                        homeControlDto.getLights().get(1).setStatus(1);
                    } else {
                        homeControlDto.getLights().get(1).setStatus(0);
                    }
                }

                try {
                    if (Objects.nonNull(homeControlDto))
                        if (status.get(1) != homeControlDto.getLights().get(1).getStatus())
                            new SaveHomeControl(getActivity()).execute(homeControlDto).get(10, TimeUnit.SECONDS);

                } catch (ExecutionException | InterruptedException | TimeoutException e) {
                    e.printStackTrace();
                }
            }
        });

        if (Objects.nonNull(homeControlDto)) {
            textViewUsgaeTimeLight1.setText("Tempo ligada: " + homeControlDto.getLights().get(0).getUsageTime() + " minutos.");
            textViewUsgaeTimeLight2.setText("Tempo ligada: " + homeControlDto.getLights().get(1).getUsageTime() + " minutos.");
            aSwitchSala.setChecked(status.get(0) == 1);
            aSwitchQuarto.setChecked(status.get(1) == 1);
        }

        try {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    actualFragment = MainActivity.getActualFragment();
                    if (actualFragment.equals("FRAGMENT_A")) {
                        MyFragmentA fragmentA = new MyFragmentA();
                        managerFragment(fragmentA, "FRAGMENT_A");
                    }
                }
            }, 10000);
        } catch (Exception e) {

        }
        return root;
    }

    private void managerFragment(Fragment fragment, String tag) {
        try {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.flContainerForFragment, fragment, tag);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }catch (Exception e){

        }
    }
}
