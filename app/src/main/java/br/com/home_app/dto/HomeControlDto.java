package br.com.home_app.dto;

import lombok.*;
import java.util.List;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HomeControlDto {

    private String id;
    private String serie;
    private String token;
    private Integer homeDoorStatus;
    private String tokenHomeDoor;
    private Integer garageDoorStatus;
    private String tokenGarageDoor;
    private List<LightDto> lights;
    private List<ClimateDto> climates;


}
