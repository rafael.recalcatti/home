package br.com.home_app.dto;

import lombok.*;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClimateDto {

    private Double temperature;
    private Double humidity;
    private Integer number;
    private String description;

}
