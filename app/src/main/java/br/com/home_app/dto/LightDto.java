package br.com.home_app.dto;

import lombok.*;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LightDto {

    private String portLight;
    private String portSensor;
    private Integer number;
    private String description;
    private Integer status;
    private Long dateTimeOn;
    private Long dateTimeOff;
    private Long usageTime;
}
