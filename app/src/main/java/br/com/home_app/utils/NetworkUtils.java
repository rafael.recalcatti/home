package br.com.home_app.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Scanner;

public class NetworkUtils {

    public static String getJSONFromAPI(String url) {

        String retorno = null;
        try {

            URL apiEnd = new URL(url);
            int codigoResposta;
            HttpURLConnection conexao;
            InputStream is;

            conexao = (HttpURLConnection) apiEnd.openConnection();
            conexao.setRequestMethod("GET");
            conexao.setReadTimeout(5000);
            conexao.setConnectTimeout(5000);
            conexao.setAllowUserInteraction(true);
            conexao.connect();

            codigoResposta = conexao.getResponseCode();

            if (codigoResposta < HttpURLConnection.HTTP_BAD_REQUEST) {
                is = conexao.getInputStream();
            } else {
                is = conexao.getErrorStream();
            }

            retorno = converterInputStreamToString(is);
            is.close();
            conexao.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return retorno;
    }

    public static String postFromAPI(String url, String json) {

        String jsonDeResposta = null;

        try {

            HttpURLConnection connection = null;  //abre conexao
            URL apiEnd = new URL(url);

            connection = (HttpURLConnection) apiEnd.openConnection();

            connection.setRequestMethod("POST"); //fala que quer um post

            connection.setRequestProperty("Content-type", "application/json"); //fala o que vai mandar

            connection.setDoOutput(true); //fala que voce vai enviar algo

            PrintStream printStream = new PrintStream(connection.getOutputStream());
            printStream.println(json); //seta o que voce vai enviar
            Log.i("JSON::", json);
            connection.connect(); //envia para o servidor

            jsonDeResposta = new Scanner(connection.getInputStream()).next(); //pega resposta

        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonDeResposta;

    }

    private static String converterInputStreamToString(InputStream is) {
        StringBuffer buffer = new StringBuffer();
        try {
            BufferedReader br;
            String linha;

            br = new BufferedReader(new InputStreamReader(is));
            while ((linha = br.readLine()) != null) {
                buffer.append(linha);
            }

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return buffer.toString();
    }
}