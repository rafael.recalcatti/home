package br.com.home_app.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import br.com.home_app.dto.HomeControlDto;
import br.com.home_app.service.HomeControlServices;


public class SaveHomeControl extends AsyncTask<HomeControlDto, Void, Boolean> {

    private ProgressDialog load;
    private Activity activity;

    public SaveHomeControl(Activity aActivity) {
        activity = aActivity;
    }

    @Override
    protected void onPreExecute() {
        load = ProgressDialog.show(activity, "", "Processando...");
    }

    @Override
    protected Boolean doInBackground(HomeControlDto... homeControlDtos) {
        homeControlDtos[0].setToken(null);
        return new HomeControlServices().postLights(homeControlDtos[0]);
    }

    protected void onPostExecute(Boolean isSucess) {

        if (!isSucess) {
            Toast.makeText(activity, "Falha ao conectar o servidor!", Toast.LENGTH_LONG).show();
        }
        load.dismiss();
    }
}