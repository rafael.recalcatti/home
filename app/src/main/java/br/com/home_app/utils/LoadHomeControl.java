package br.com.home_app.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import java.util.Objects;

import br.com.home_app.dto.HomeControlDto;
import br.com.home_app.service.HomeControlServices;


public class LoadHomeControl extends AsyncTask<Void, Void, HomeControlDto> {

    private ProgressDialog load;
    private Activity activity;

    public LoadHomeControl(Activity aActivity) {
        activity = aActivity;
    }

    @Override
    protected HomeControlDto doInBackground(Void... voids) {
        return new HomeControlServices().getHomeControl("001");
    }

    @Override
    protected void onPreExecute() {
        if (Objects.nonNull(activity))
            load = ProgressDialog.show(activity, "Por favor Aguarde ...", "Atualizando Informações...");
    }

    protected void onPostExecute(HomeControlDto HomeControlDto) {
        if (Objects.nonNull(activity))
            load.dismiss();
    }
}