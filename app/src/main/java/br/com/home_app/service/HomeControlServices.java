package br.com.home_app.service;


import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import br.com.home_app.dto.HomeControlDto;
import br.com.home_app.utils.NetworkUtils;

public class HomeControlServices {

    public HomeControlDto getHomeControl(String serie) {

        HomeControlDto homeControlDto = null;
        String endPoint = "https://home-app-ws.herokuapp.com/home_control/" + serie;
        ObjectMapper mapper = new ObjectMapper();

        try {
            String json = NetworkUtils.getJSONFromAPI(endPoint);
            homeControlDto = mapper.readValue(json, HomeControlDto.class);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return homeControlDto;
    }

    public Boolean postLights(HomeControlDto lightsDto) {

        String endPoint = "https://home-app-ws.herokuapp.com/home_control";
        ObjectMapper mapper = new ObjectMapper();
        String response = null;

        try {
            response = NetworkUtils.postFromAPI(endPoint, mapper.writeValueAsString(lightsDto));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Boolean.parseBoolean(response);
    }

}